import {
	Component, OnInit, ViewChildren, ViewChild, AfterViewInit, QueryList, ElementRef
} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {MatListItem, MatList} from '@angular/material/list';

import { DialogUserComponent } from './dialog-user/dialog-user.component';
import { DialogUserType } from './dialog-user/dialog-user-type.enum';
import { Action } from './shared/model/action.enum';
import { Event } from './shared/model/event.enum';
import { Message } from './shared/model/message.interface';
import { User } from './shared/model/user.interface';
import { SocketService } from './shared/services/socket.service';


@Component({
	selector: 'app-chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.css']
})

export class ChatComponent implements OnInit {
	action = Action;
	user: User;
	messages: Message[] = [];
	messageContent: string = '';
	ioConnection: any;
	dialogRef: MatDialogRef<DialogUserComponent>|null;
	defaultDialogUserParams: any = {
		disableClose: true,
		data: {
			title: 'Welcome!',
			dialogType: DialogUserType.NEW
		}
	};
	@ViewChild(MatList, { read: ElementRef }) matList: ElementRef;
	@ViewChildren(MatListItem, { read: ElementRef }) matListItems: QueryList<MatListItem>;

	constructor(
		private socketService: SocketService,
		public dialog: MatDialog
	) {}
	ngOnInit() {
		this.initModel();
		setTimeout(() => this.openUserPopup(this.defaultDialogUserParams), 0);
	}

	ngAfterInitView() {
		this.matListItems.changes
			.subscribe(_ => this.scrollToBottom());
	}
	private scrollToBottom() {
		try {
			this.matList.nativeElement.scrollTop = this.matList.nativeElement.scrollHeight;
		}
		catch(error) {};
	}

	private initModel() {
		this.user = {
			id: this.getRandomId()
		}
	}

	private getRandomId(min: number = 1, max: number = 10000000): number {
		return Math.floor(Math.random() * max) + min;
	}

	public onClickUserInfo() {
		this.openUserPopup({
			data: {
				username: this.user.name,
				title: 'Edit Details',
				dialogType: DialogUserType.EDIT
			}
		});
	}

	private initIoConnection(): void {
		this.socketService.initSocket();
		this.ioConnection = this.socketService.onMessage()
			.subscribe((msg: Message) => this.messages.push(msg));
		this.socketService.onEvent(Event.CONNECT)
			.subscribe(() => console.log('Connection successful.'));
		this.socketService.onEvent(Event.DISCONNECT)
			.subscribe(() => console.log('Disconnected.'));
	}

	private openUserPopup(params): void {
		this.dialogRef = this.dialog.open(DialogUserComponent, params);
		this.dialogRef.afterClosed().subscribe(paramsDialog => {
			if(!paramsDialog) return;
			this.user.name = paramsDialog.username;
			if(paramsDialog.dialogType === DialogUserType.NEW) {
				this.initIoConnection();
				this.sendNotification(paramsDialog, Action.JOINED);
			}
			if(paramsDialog.dialogType === DialogUserType.EDIT)
				this.sendNotification(paramsDialog, Action.RENAME);
		});
	}

	public sendMessage(message: string): void {
		if (!message.trim()) return;

		this.socketService.send({
			from: this.user,
			content: message
		});
		this.messageContent = null;
	}
	public sendNotification(params: any, action: Action): void {
		let msg: Message;
		if(action === Action.JOINED) {
			msg = {
				from: this.user,
				action: action
			}
		}
		if(action === Action.RENAME) {
			msg = {
				action: action,
				content: {
					username: this.user.name,
					previousUsername: params.previousUsername
				}
			};
		}
		this.socketService.send(msg);
	}
}
