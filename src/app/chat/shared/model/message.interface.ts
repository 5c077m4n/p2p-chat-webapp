import {User} from './user.interface';
import {Action} from './action.enum';

export interface Message {
    from?: User;
    content?: any;
    action?: Action;
}
